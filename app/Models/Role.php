<?php

namespace App\Models;

use Laratrust\Models\LaratrustRole;

class Role extends LaratrustRole
{
    public const BALONCUK = 2;
    public const BANDROLTAKIP = 3;
    public const OFISSIZLER_WEB = 4;

    public $guarded = [];
}
