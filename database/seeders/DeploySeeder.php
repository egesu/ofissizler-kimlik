<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

use App\Models\Role;

class DeploySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::firstOrCreate([
            'id' => Role::BALONCUK,
            'name' => 'baloncuk',
            'display_name' => 'Baloncuk',
        ]);

        Role::firstOrCreate([
            'id' => Role::BANDROLTAKIP,
            'name' => 'bandrol_takip',
            'display_name' => 'Bandrol Takip',
        ]);

        Role::firstOrCreate([
            'id' => Role::OFISSIZLER_WEB,
            'name' => 'ofissizler_web',
            'display_name' => 'Ofissizler.org',
        ]);
    }
}
