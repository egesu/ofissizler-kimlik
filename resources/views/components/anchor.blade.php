<a {{ $attributes->merge(['class' => 'inline-flex items-center text-lime-500 hover:text-lime-400 active:underline active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:ring ring-gray-300 disabled:opacity-25 transition ease-in-out duration-150 decoration-dotted font-semibold']) }}>
    {{ $slot }}
</a>
