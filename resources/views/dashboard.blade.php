<x-app-layout>
  <x-slot name="header">
    <h2 class="font-semibold text-xl text-zinc-800 leading-tight dark:text-zinc-300">
      {{ __('Merhaba') }}
    </h2>
  </x-slot>

  <div class="py-3">
    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
      <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg dark:bg-zinc-700">
        <div class="p-6 border-zinc-200 dark:text-lime-100">
          <h2 class="font-semibold text-2xl leading-tight">Baloncuk</h2>
          <p class="my-3">Baloncuk, ofissizlere özel bir mesajlaşma ortamıdır. İçerideki bol miktardaki odadan herhangi birine girerek mesajlaşmaya başlayabilirsiniz. Meslek özelindeki odalardan geyik muhabbetine, hukuktan muhasebeye çeşitli konularda ofissiz çalışanlarla fikir alışverişinde bulunabilirsiniz.</p>
          <p class="my-3">
            <strong>Ofissizler Kimlik</strong> hesabınla baloncuğa tek tıkla girebilirsin.
          </p>

          <p class="my-3">
            Baloncuk'a girmek için aşağıdaki uygulamalardan işletim sistemine uygun olanı indir.
            <br>
            <strong class="text-red-400 font-bold text-shadow">ÖNEMLİ:</strong> Giriş yaparken <em>Homeserver/Sunucu</em> kısmını değiştirmeli ve <em>baloncuk.ofissizler.org</em> girmelisiniz.

            <ul>
              <li>
                <x-anchor href="https://element.io/get-started" target="_blank">
                  Bilgisayar uygulamasını indir
                </x-anchor>
              </li>
              <li>
                <x-anchor href="https://play.google.com/store/apps/details?id=im.vector.app" target="_blank">
                  Android uygulamasını indir
                </x-anchor>
              </li>
              <li>
                <x-anchor href="https://apps.apple.com/us/app/riot-im/id1083446067" target="_blank">
                  iOS uygulamasını indir
                </x-anchor>
              </li>
              <li>
                <x-anchor href="https://app.element.io" target="_blank">
                  Web üzerinden gir
                </x-anchor>
              </li>
            </ul>
          </p>

          <a class="my-3 inline-flex items-center px-4 py-2 bg-red-700 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-red-500 active:bg-red-900 focus:outline-none focus:border-gray-900 focus:ring ring-gray-300 disabled:opacity-25 transition ease-in-out duration-150" href="element://vector/webapp/">
            Baloncuğa Gir
          </a>

          <p class="my-3">
            Halihazırda baloncuğa girdiysen <strong>#sohbet</strong> odasına katılmak için <x-anchor target="_blank" href="https://matrix.to/#/#sohbet:baloncuk.ofissizler.org">tıkla</x-anchor>.
          </p>
        </div>
      </div>
    </div>
  </div>
</x-app-layout>
